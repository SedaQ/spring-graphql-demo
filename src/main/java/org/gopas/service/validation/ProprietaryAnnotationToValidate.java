package org.gopas.service.validation;

import org.gopas.service.LicenseKeyValidator;

import javax.validation.Constraint;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = LicenseKeyValidator.class)
public @interface ProprietaryAnnotationToValidate {
}
