package org.gopas.service.mappers;

import org.gopas.api.AddressDetailedViewDto;
import org.gopas.data.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressDetailedViewDto mapToDetailedView(Address address);
}
