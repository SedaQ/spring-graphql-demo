package org.gopas.service;

import org.gopas.api.PersonBasicViewDto;
import org.gopas.api.PersonCreateDto;
import org.gopas.api.PersonDetailedViewDto;
import org.gopas.data.entity.Person;
import org.gopas.data.repository.PersonRepository;
import org.gopas.service.exceptions.ResourceNotFoundException;
import org.gopas.service.mappers.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {

    private PersonRepository personRepository;
    private PersonMapper personMapper;

    @Autowired
    public PersonService(PersonRepository personRepository,
                         PersonMapper personMapper) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
    }

    public Person findByEmail(String email) {
        return personRepository.findByEmail(email)
                .orElseThrow(
                        () -> new ResourceNotFoundException("Person with email: " + email + " was not found."));
    }

    public void createPerson(PersonCreateDto personCreateDto) {
        Person person = personMapper.mapToPerson(personCreateDto);
        personRepository.save(person);
    }

    @Transactional
//    @PreAuthorize("hasRole('ROLE_READER')")
    public Page<PersonBasicViewDto> findAll(Pageable pageable) {
        Page<Person> persons = personRepository.findAll(pageable);
        return personMapper.mapToPageDto(persons);
    }

    @Transactional
    public PersonDetailedViewDto findById(Long id) {
        Person person = personRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Person with id: " + id + " was not found."));
        return personMapper.mapToDetailView(person);
    }


    // ModelMapper, Dozer -> uses reflection
    // MapStruct -> uses "manual" mapping
}
