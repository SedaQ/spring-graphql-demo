package org.gopas.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.gopas.api.PersonCreateDto;
import org.gopas.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonMutation implements GraphQLMutationResolver {

    @Autowired
    private PersonService personService;

    public String createPerson(PersonCreateDto personCreateDto) {
        personService.createPerson(personCreateDto);
        return personCreateDto.toString();
    }

    public Long deletePerson(Long id) {
        // missing implementation
        return id;
    }
}
