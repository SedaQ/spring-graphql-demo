package org.gopas.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.gopas.api.PersonBasicViewDto;
import org.gopas.api.PersonDetailedViewDto;
import org.gopas.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PersonQuery implements GraphQLQueryResolver {

    @Autowired
    private PersonService personService;

    public PersonDetailedViewDto personById(Long id) {
        return personService.findById(id);
    }

    public List<PersonBasicViewDto> persons() {
        return personService.findAll(null).getContent();
    }

}
